# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_20_074747) do

  create_table "group_events", force: :cascade do |t|
    t.string "name", limit: 50
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer "duration"
    t.text "description"
    t.string "location", limit: 150
    t.integer "status", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_group_events_on_deleted_at"
    t.index ["location"], name: "index_group_events_on_location"
    t.index ["name"], name: "index_group_events_on_name"
    t.index ["start_date"], name: "index_group_events_on_start_date"
  end

end
