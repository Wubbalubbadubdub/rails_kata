class AddIndexesToGroupEvent < ActiveRecord::Migration[6.0]
  def change
    add_index :group_events, :name
    add_index :group_events, :start_date
    add_index :group_events, :location
  end
end
