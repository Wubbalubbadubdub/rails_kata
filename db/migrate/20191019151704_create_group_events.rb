class CreateGroupEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :group_events do |t|
      t.string :name, limit: 50
      t.datetime :start_date
      t.datetime :end_date
      t.integer :duration
      t.text :description
      t.string :location, limit: 150
      t.integer :status, default: 0
      t.timestamps
    end
  end
end
