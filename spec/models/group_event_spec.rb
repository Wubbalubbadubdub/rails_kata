require 'rails_helper'
require 'date'

RSpec.describe GroupEvent, type: :model do

  describe "before validation" do
    it "calculates period variables" do 
      group_event = GroupEvent.new
      expect(group_event).to receive(:calculates_period_variables)
      group_event.valid?
    end
  end
  
  describe "before publication" do
    it "validates it has all the fields" do
      group_event = GroupEvent.new(:status => :published)
      group_event.should validate_presence_of(:name)
      group_event.should validate_presence_of(:description)
      group_event.should validate_presence_of(:location)
      group_event.should validate_presence_of(:start_date)
      group_event.should validate_presence_of(:end_date)
      group_event.should validate_presence_of(:duration)
    end
  end

  describe "#difference in days" do
    it "calculates number of days between two dates" do
      first_date = Date.today
      second_date = Date.today + 2.days

      group_event = GroupEvent.new(:start_date => first_date, :end_date => second_date)
      
      expect(group_event.difference_in_days).to eq(2)
    end
  end

  describe "#is_published?" do
    it "validates that group event has published status" do
      group_event = GroupEvent.new(:status => :published)

      expect(group_event.is_published?).to eq(true)
    end
  end

  describe "#calculates_period_variables" do
    it "calculates end_date if duration is set" do
      group_event = GroupEvent.new(:start_date => Date.today, :duration => 2)

      group_event.calculates_period_variables

      expect(group_event.end_date).to eq(Date.today + 2)
    end

    it "calculates duration if end_date is set" do
      group_event = GroupEvent.new(:start_date => Date.today, :end_date => Date.today + 2)

      group_event.calculates_period_variables

      expect(group_event.duration).to eq(2)
    end
  end
end
