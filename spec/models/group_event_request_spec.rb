require 'rails_helper'
require 'securerandom'

RSpec.describe GroupEventRequest, type: :model do
  
  describe "should validate" do
    let(:group_event_request){
      build(:group_event_request)
    }
    
    it "presences of start_date" do 
      expect(group_event_request).to receive(:presence_of_start_date)
      group_event_request.valid?
    end

    it "presence of end_date xor duration" do 
      expect(group_event_request).to receive(:presence_of_end_date_xor_duration)
      group_event_request.valid?
    end
  end

  describe "#initialize" do
    it "populates all attributes of model with values from params attached to key that is same as attribute name" do 
      params = Hash.new(
        :start_date => Date.today,
        :end_date => Date.today,
        :duration => rand(10),
        :name => SecureRandom.uuid,
        :location => SecureRandom.uuid,
        :description => SecureRandom.uuid,
        :status => :published
      )

      group_event_request = GroupEventRequest.new(params)

      params.each do |key, value| 
        expect(value).to eq(group_event_request.instance_variable_get(key))
      end
    end
  end

  describe "#presence_of_end_date_xor_duration" do
    it "adds error to the model if both end_date and duration are present" do
        group_event_request = GroupEventRequest.new(:end_date => Date.today, :duration => 5)
        error_message = "only one of either end_date or duration are allowed in the combination with start_date"

        group_event_request.valid?

        expect(group_event_request.errors.messages[:base]).to include(error_message)
    end 
  end

  describe "#presence_of_start_date" do
    it "adds error to the model if start_date is not supplied in combination with either end_date or duuration" do
      group_event_request = GroupEventRequest.new(:end_date => Date.today)
      error_message = "must be present if either end_date or duration are supplied"

      group_event_request.valid?

      expect(group_event_request.errors.messages[:start_date]).to include(error_message)
    end
  end
end
