FactoryBot.define do
  
    factory :group_event do
      name {Faker::Name.name }
      description {Faker::ChuckNorris.fact}
      status {:draft}
      location {Faker::TvShows::FamilyGuy.location}

      factory  :invalid_group_event do 
        end_date {Date.today + 3}
      end

      factory :published_group_event do 
        status {"published"}
      end

      factory  :valid_group_event do 
        start_date {Date.today}
        end_date {Date.today + 3}
      end
    end

    
    factory :group_event_request do
        name {Faker::Name.name }
        description {Faker::ChuckNorris.fact}
        status {:draft}
        location {Faker::TvShows::FamilyGuy.location}
        end_date {Date.today + 3}
        start_date {Date.today}
        duration {3}

        initialize_with { 
            GroupEventRequest.new({
                :name => name ,
                :description => description ,
                :status =>  status ,
                :location => location ,
                :end_date => end_date ,
                :start_date => start_date ,
                :duration => duration }) }
      end
  end