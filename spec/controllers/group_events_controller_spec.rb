require 'rails_helper'

RSpec.describe GroupEventsController, type: :controller do
    describe "GET index" do
        it "returns ok status" do 
            get :index
            expect(response).to have_http_status(:ok)
        end

        it "assings group_events" do 
            group_event = create(:group_event)
            get :index
            expect(assigns(:group_events)).to eq([group_event])
            expect(response.body).to eq([group_event].to_json)
        end
    end

    describe "GET show" do 
        it "assings group_event" do 
            group_event = create(:group_event)
            get :show,  params: { id: group_event.id }
            expect(assigns(:group_event)).to eq(group_event)
            expect(response.body).to eq(group_event.to_json)
        end
    end

    describe "POST create" do
        it "creates new group event if request is valid" do 
            expect { post "create", params:  {group_event: attributes_for(:valid_group_event)}}.to  change(GroupEvent, :count).by(+1)
            expect(response).to have_http_status :created
        end

        it "returns bad request if request is not valid " do
            expect { post "create", params:  {group_event: attributes_for(:invalid_group_event)}}.not_to change(GroupEvent, :count)
            expect(response).to have_http_status :bad_request
        end
    end

    describe "PUT update" do
        it "updates if request is valid" do 
            group_event = create(:group_event)
            updated_name = 'Updated!'
            put "update", params:  {
                id: group_event.id,
                group_event: attributes_for(:group_event, name: updated_name)}
            group_event.reload
            
            expect(response).to have_http_status :ok
            expect(group_event.name).to eq(updated_name)
        end

        it "returns bad request if request is invalid" do 
            group_event = create(:group_event)
            put "update", params:  {
                id: group_event.id,
                group_event: attributes_for(:invalid_group_event)}
            
            expect(response).to have_http_status :bad_request
        end

        it "returns unprocessable entity if event is to be published without all fields" do 
            group_event = create(:group_event)
            put "update", params:  {
                id: group_event.id,
                group_event: attributes_for(:published_group_event)}

            expect(response).to have_http_status :unprocessable_entity
        end
    end

    describe "DELETE destroy " do 
        it "deletes group_event from database" do
            group_event = create(:group_event)

            delete :destroy , params: { id: group_event.id }

            expect(GroupEvent.exists?(group_event.id)).to be_falsy
        end
    end
end
