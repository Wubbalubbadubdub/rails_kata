class GroupEvent < ApplicationRecord
    acts_as_paranoid
    validates_presence_of :name, :description, :location, :start_date, :end_date, :duration, :if =>  :is_published?
    before_validation :calculates_period_variables
    enum status: [:draft, :published]

    attr_accessor  :start_date, :end_date, :duration

    def calculates_period_variables
        if @duration 
            @end_date = @start_date.to_date.next_day(@duration.to_i)
        elsif @end_date
            @duration = difference_in_days
        end
    end

    def difference_in_days
        (@end_date.to_date - @start_date.to_date ).to_i
    end

    def is_published?
        self.status == "published"
    end
end
