class GroupEventRequest
    include ActiveModel::Validations
    include ActiveModel::AttributeMethods

    validate :presence_of_start_date 
    validate :presence_of_end_date_xor_duration 

    attr_accessor :start_date, :end_date, :duration
    attr_accessor :name, :location, :description, :status

    def initialize(group_event_request_params)
        @start_date = group_event_request_params[:start_date]
        @end_date = group_event_request_params[:end_date]
        @duration = group_event_request_params[:duration]
        @name = group_event_request_params[:name]
        @location = group_event_request_params[:location]
        @description = group_event_request_params[:description]
        @status = group_event_request_params[:status]
    end

    def presence_of_end_date_xor_duration
        errors.add(:base, "only one of either end_date or duration are allowed in the combination with start_date") if @end_date && @duration
    end

    def presence_of_start_date
        errors.add(:start_date , "must be present if either end_date or duration are supplied") if !@start_date && (@end_date || @duration)
    end
end